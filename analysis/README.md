## About
___
* **Technologies**: Python, pandas, matplotlib
* **Developer**: Martin Hofbauer [@xhofb1](https://gitlab.fi.muni.cz/xhofb1)

## Description
___
These are a set of analytics scripts for logs from the INJECT platform. The scripts perform analytics on logs obtained from TTXs ran in INJECT.
___
## Requirements
___
* **Language**: Python 3.8 or higher
* **Modules**: [pandas](https://pandas.pydata.org/) and [matplotlib](https://matplotlib.org/)
* **Other requirements**: directory with logs should be named `logs`, placed in the parent directory of `analysis`. Description of directory structure can be found [here](#logs-description).
___ 
## How to use
___
1. Enter the directory `analysis`.
2. Run command `python3 main.py` with appropriate flags and number of teams for analysis (*Note*: if you are using Windows, use `py main.py` with same flags).

List of flags:
* `-p`, `--plot`: This flag will cause the script to export plots as `png` files. These plots can be located in `plots` directory in `analysis` parent directory.
* `-c`, `--csv`: This flag will cause the script to export DataFrames as `csv` files. These files can be located in `data` directory in `analysis` parent directory.
* `--correct`: This flag will run only analytics of correct usage of tools.
* `--completed`: This flag will run only analytics of exercise completion time.
* `--time`: This flag will run only analytics of milestone completion time (except of time for exercise completion).
* `--count`: This flag will run only analytics of milestone reach count.
* `--usage`: This flag will run only analytics of tool usage.
* `-a, --all`: This flag will run all available analytics.

Mandatory parameter:
* N(int): number of team the analytics should run on (first *N* teams will be used).

Example use:
`python3 main.py -p -c -a 5`
This command will export plots and csv files of analysis for first 5 teams from logs.

*Note*: You should always use at least one of flags `-p` and `-c`. Otherwise the script will run, but will not export any data.
*Note*: You should always use at least one of these flags: `--correct`, `--completed` , `--time`, `--count`, `--usage`, `-a`, and `--all`. While the scipt will run without any of these flags, it will not perform any analysis.
*Note*: If directories `plots` or `data` are not present when the script runs, the script will create them.
*Note*: The script will likely display a warning that you are creating too many plots when using the `--all` flag. Ignore it.
___
## *Logs* description
The directory structure of directory `logs` should be as follows:
* `team-{number}` (dir)
* `email_participants.jsonl`
* `exercise_inject_categories.jsonl`
* `exercsie_milestones.jsonl`
* `exercise_tools.jsonl`
* `exercise.jsonl`
* `teams.jsonl`

The directory structure of directory `team-{number}` should look like this:
* `uploaded_files` (dir)
* `action_logs.jsonl`
* `emails.jsonl`
* `inject_categories.jsonl`
* `milestones.jsonl`

The directory `uploaded_files` contains all files uploaded by teams during the exercise. These are not used for analysis.

*Note*: There can be multiple directories `team-{number}`.

## Additional notes
______
If there are empty rows in `csv` files generated by the script, these are there representing situations, where entity (most likely milestone) exists but was not reached by any team.