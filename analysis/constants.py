import os

MILESTONE_LOGS = {
    "teams": ["milestones.jsonl"],
    "overall": ["exercise_milestones.jsonl"],
}

TOOLS_LOG = {"teams": ["action_logs.jsonl"], "overall": ["exercise_tools.jsonl"]}

PLOTS_FOLDER = os.path.join("..", "plots")
CSV_FOLDER = os.path.join("..", "data")
